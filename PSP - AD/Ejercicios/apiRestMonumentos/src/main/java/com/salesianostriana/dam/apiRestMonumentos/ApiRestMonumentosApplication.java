package com.salesianostriana.dam.apiRestMonumentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestMonumentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestMonumentosApplication.class, args);
	}

}
