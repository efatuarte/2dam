package com.salesianostriana.dam.apiRestMonumentos;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/monumentos")
@RequiredArgsConstructor
public class MonumentoController {

    private final MonumentoRepository repository;

    // Obtener la lista de todos los monumentos
    @GetMapping("/")
    public List<Monumento> findAll() {
        return repository.findAll();
    }

    // Obtener un monumento por su ID
    @GetMapping("/{id}")
    public Monumento findOne(@PathVariable("id") Long id) {
        return repository.findById(id).orElse(null);
    }

    // Crear un monumento
    @PostMapping("/")
    public ResponseEntity<Monumento> create(@RequestBody Monumento monumento) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(repository.save(monumento));
    }

    // Actualizar un monumento
    @PutMapping("/{id}")
    public Monumento edit(@RequestBody Monumento monumento, @PathVariable("id") Long id) {
        Monumento monumentoAntiguo = repository.findById(id).orElse(monumento);

        monumentoAntiguo.setCodigo_pais(monumento.getCodigo_pais());
        monumentoAntiguo.setNombre_pais(monumento.getNombre_pais());
        monumentoAntiguo.setNombre_ciudad(monumento.getNombre_ciudad());
        monumentoAntiguo.setNombre(monumento.getNombre());
        monumentoAntiguo.setDescripcion(monumento.getDescripcion());
        monumentoAntiguo.setFoto(monumento.getFoto());

        return repository.save(monumentoAntiguo);
    }

    // Eliminar un monumento
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        repository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
